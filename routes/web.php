<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/cars', 'CarsController@index');
$router->post('/cars', 'CarsController@store');
$router->patch('/cars-find-by-licence', 'CarsController@findByLicence');
$router->patch('/cars/{id}', 'CarsController@update');