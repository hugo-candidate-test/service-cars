<?php

namespace App\Http\Controllers;
use App\Car;
use App\Traits\ApiResponser\Success;
use App\Traits\ApiResponser\Error;
use Illuminate\Http\Request;
use App\Http\Resources\CarResource;
use Illuminate\Database\QueryException;

use MongoDB\Driver\Exception\BulkWriteException as MongoError;


class CarsController extends Controller
{
    use Success;
    use Error;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public  function index()
    {
        $cars =  Car::all();
        return $this->successResponse($cars);
    }

    public  function store(Request $request)
    {
        try {
                $data = [
                    'license_plate' => $request->license_plate,
                    'type_of_car' => $request->type_of_car
                ];
            
                $car = Car::where('license_plate', $request->license_plate)->first();

                if (! is_null($car)){
                    $dataResponse = new CarResource($car);
                    return $this->successResponse($dataResponse);
                }

                $car = Car::create($data);
                $dataResponse = new CarResource($car);
                return $this->successResponse($dataResponse);

        } catch (MongoError $exception) {
            return $this->errorResponse(['Error created Ticket']);
        }
    }

      public  function findByLicence(Request $request)
    {

        $car = Car::where('license_plate', $request->license_plate)->first();
        if (! is_null($car)){
            $dataResponse = new CarResource($car);
            return $this->successResponse($dataResponse);
        }
        return $this->errorResponse([], 404, 'Record Not Found');
       
    }


    public  function update(Request $request)
    {
        try {
                $data = [
                    'start_time' => $request->start_time,
                    'end_time' => $request->end_time,
                    'license_plate' => $request->license_plate,
                    'type_of_car' => $request->type_of_car
                ];
            
                $car = Car::find($request ->id);

                if ($car->update(array_filter($data))){
                   return $this->successResponse('', 204);
                }
              
                return $this->errorResponse(['Error created Ticket']);

        } catch (MongoError $exception) {
            return $this->errorResponse(['Error created Ticket']);
        }
    }


    
}
