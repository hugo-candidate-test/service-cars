<?php

namespace App\Http\Middleware;

use Closure;

class AuthApiGateway
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if ($request->header('Authorization') == env('GATEWAY_SECRET_KEY') ){
            return $next($request);
        }
        abort(401, 'Unauthorized Error');
    }
}
