<?php

namespace App;
// use Illuminate\Database\Eloquent\Model;
use  \Jenssegers\Mongodb\Eloquent\Model;

class Car extends Model 
{

 protected $collection = 'cars';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'license_plate', 'type_of_car'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];
}
